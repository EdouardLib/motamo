from django.db import models
from django.contrib.auth.models import User
import datetime

# Enums
class LightColors:
    RED = 1
    DEEP_RED = 2
    BLUE = 3
    ROYAL_BLUE = 4
    WHITE = 5
    UV = 6

class Periode:
    DAY = 60*60*24
    WEEK = 60*60*24*7
    MONTH = 60*60*24*31
    FOUR_MONTH = 60*60*24*122
    ONE_YEAR = 60*60*24*365
    WHOLE_PERIODE = 0


# Users
class AgromeUser(models.Model):
    user = models.OneToOneField(User,default=None)
    #birthday = models.DateField(default=None)
    newsletter = models.BooleanField(default=False)
    experience = models.IntegerField(default=1)
    level = models.IntegerField(default=1)
    avatar = models.ImageField(default=None)

    def __str__(self):
        return "Profil of {0}".format(self.user.username)



# agrome
class Agrome(models.Model):
    uid = models.IntegerField(default=None)
    name = models.TextField(default='agrome box')
    dateCreate = models.DateTimeField(auto_now_add=True)
    dateUpdate = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, default=None, null=True)
    isActivated = models.BooleanField(default=False)
    isServerSynchronised = models.BooleanField(default=False) #First connection agrome - server

    # TD: method to log dateUpdate & create while saving value

#Famille
class Family(models.Model):
    name = models.CharField(max_length=60)
    image = models.ImageField(default=None,upload_to="preview",null=True)
    description = models.TextField(default="Not available")

#Specie
class Specie(models.Model):
    name = models.CharField(max_length=60)
    image = models.ImageField(default=None,upload_to="preview", null=True) # TODO: change paths
    description = models.TextField(default="Not available")
    family = models.ForeignKey(Family, null=True)

#Plant
class Plant(models.Model):
    name = models.CharField(max_length=60)
    image = models.ImageField(default=None,upload_to="preview", null=True) # TODO: change path
    description = models.TextField(default="Not available")
    specie = models.ForeignKey(Specie, null=True)

#Cycle -> Differents tests
class Cycle(models.Model):
    isModel = models.BooleanField(default=False)
    isOfficial = models.BooleanField(default=False)
    isActive = models.BooleanField(default=False)
    isPaired = models.BooleanField(default=False)
    version = models.IntegerField(default=1) # agrome use it to check if must update or not
    currentTime = models.IntegerField(default=0)
    duration = models.BigIntegerField(default=2592000)
    name = models.CharField(max_length=60)
    description = models.TextField(default="Not available")
    plant = models.ForeignKey(Plant, null=True)
    user = models.ForeignKey(User, null = True, default=None) # If not null, it's a user cycle model
    agrome = models.ForeignKey(Agrome, null = True, default=None) # If not null, it's an agrome user cycle


    def duplicateForUser(self,user):
        userCycle = self
        userCycle.fk = None # Create a duplicate
        userCycle.isOfficial = False
        userCycle.isModel = False
        userCycle.isActive = False
        userCycle.user = user
        userCycle.save()

        phases = Phase.objects.filter(cycle=self).all()
        for phase in phases :
            phase.duplicateForCycle(userCycle)

        return userCycle

    def duplicateForAgrome(self,user,agrome):
        userCycle = self

        if userCycle.user == None: # Always create duplicate as user model
            userCycle = self.duplicateForUser(user)

        agromeCycle = userCycle
        agromeCycle.pk = None
        agromeCycle.isActive = True
        agromeCycle.agrome = agrome
        agromeCycle.save()

        phases = Phase.objects.filter(cycle=userCycle).all()
        for phase in phases :
            phase.duplicateForCycle(agromeCycle)

        return agromeCycle

#Phase
class Phase(models.Model):
    cycle = models.ForeignKey(Cycle, null=True)
    name = models.CharField(max_length=60)
    description = models.TextField(default="Not available")
    phaseTimeStart = models.FloatField(default=0)
    phaseTimeStop = models.FloatField(default=0)
    # Expectation
    size = models.FloatField(default=None)
    airHumidity = models.FloatField(default=None)
    airTemperature = models.FloatField(default=None)

    def getPhaseTimeString(self):
        return datetime.datetime.fromtimestamp(float(self.phaseTime))

    def duplicateForCycle(self, userCycle):
        userPhase = self
        userPhase.pk = None
        userPhase.cycle = userCycle

        userWaterParameter = WaterParameter.objects.first(phase=self).first()
        userWaterParameter.fk = None
        userWaterParameter.save()

        userLightParameter = LightParameter.objects.first(phase=self).first()
        userLightParameter.fk = None
        userLightParameter.save()

        userPhase.save()

        return userPhase

    def getPhasesForPeriod(self, cycle, periode):
        if periode == Periode.DAY :
            phases = Phase.objects.filter(cycle = cycle).filter(phaseTimeStart_lte = Periode.DAY).all()
        elif periode == Periode.WEEK:
            phases = Phase.objects.filter(cycle = cycle).filter(phaseTimeStart_lte = Periode.MONTH ).all()
        elif periode == Periode.MONTH:
            phases = Phase.objects.filter(cycle = cycle).filter(phaseTimeStart_lte = Periode.MONTH ).all()
        elif periode == Periode.FOUR_MONTH:
            phases = Phase.objects.filter(cycle = cycle).filter(phaseTimeStart_lte = Periode.FOUR_MONTH ).all()
        elif periode == Periode.ONE_YEAR:
            return Phase.objects.filter(cycle = cycle).filter(phaseTimeStart_lte = Periode.ONE_YEAR ).all()
        elif periode == Periode.WHOLE_PERIODE:
            return Phase.objects.filter(cycle = cycle).all()
        return None

# Water
class WaterParameter(models.Model):
    phase = models.ForeignKey(Phase, default=None, null=True)
    wateringTime = models.IntegerField(default = 2)
    waitingTime = models.IntegerField(default = 60*5)


class Water(models.Model):
    agromeUID = models.IntegerField(default=0)# TD: delete it
    cycle = models.ForeignKey(Cycle, default=None, null=True)
    timestamp = models.IntegerField(default=0)
    value = models.FloatField(default=0)

# Light
class LightParameter(models.Model):
    phase = models.ForeignKey(Phase, default=None, null=True)
    color = models.IntegerField(default = LightColors.RED)
    intensity = models.IntegerField(default=75)
    timeStart = models.IntegerField(default = 0)
    timeStop = models.IntegerField(default = 1200)


class Light(models.Model):
    agromeUID = models.IntegerField(default=0)# TD: delete it
    cycle = models.ForeignKey(Cycle, default=None, null=True)
    timestamp = models.IntegerField(default=0)
    color = models.IntegerField(default=LightColors.RED)
    value = models.FloatField(default=0)


# Size
class Size(models.Model):
    agromeUID = models.IntegerField(default=0)# TD: delete it
    cycle = models.ForeignKey(Cycle, default=None, null=True)
    timestamp = models.IntegerField(default=0)
    value = models.FloatField(default=0)

    def getSizeList(self,cycle,timeMax):
        if(timeMax == Periode.WHOLE_PERIODE):
            sizes = Size.objects.filter(cycle = cycle).all()
        else:
            sizes = Size.objects.filter(cycle=cycle).filter(timestamp__lte = timeMax).all()#.order_by("timestamp")
        return [(size.value,size.timestamp) for size in sizes]  # TODO: Order by timestamp

# Air temperature
class AirTemperature(models.Model):
    agromeUID = models.IntegerField(default=0)# TD: delete it
    cycle = models.ForeignKey(Cycle, default=None, null=True)
    timestamp = models.IntegerField(default=0)
    value = models.FloatField(default=0)

# Air humidity
class AirHumidity(models.Model):
    agromeUID = models.IntegerField(default=0)# TD: delete it
    cycle = models.ForeignKey(Cycle, default=None, null=True)
    timestamp = models.IntegerField(default=0)
    value = models.FloatField(default=0)

# Camera
class Camera(models.Model):
    agromeUID = models.IntegerField(default=0)# TD: delete it
    cycle = models.ForeignKey(Cycle, default=None, null=True)
    timestamp = models.IntegerField(default=0)
    value = models.ImageField(default=None,upload_to="preview")








# BI IDEAS
#
# - Make an average of the AgromePhase to update the reference Phase
# - Give a level of cycle trustness following how big the data is
# -
#