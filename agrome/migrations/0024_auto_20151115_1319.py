# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0023_auto_20151115_1316'),
    ]

    operations = [
        migrations.AddField(
            model_name='lightparameter',
            name='timeStart',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='lightparameter',
            name='timeStop',
            field=models.IntegerField(default=1200),
        ),
    ]
