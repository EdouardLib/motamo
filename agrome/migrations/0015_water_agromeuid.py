# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0014_auto_20151102_2312'),
    ]

    operations = [
        migrations.AddField(
            model_name='water',
            name='agromeUID',
            field=models.IntegerField(default=0),
        ),
    ]
