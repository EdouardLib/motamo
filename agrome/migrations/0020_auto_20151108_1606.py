# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agrome', '0019_auto_20151108_0952'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agrome',
            name='agromeUser',
        ),
        migrations.AddField(
            model_name='agrome',
            name='isActivated',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='agrome',
            name='user',
            field=models.ForeignKey(default=None, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
