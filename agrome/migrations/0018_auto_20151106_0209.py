# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0017_auto_20151106_0133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agrome',
            name='uid',
            field=models.IntegerField(default=None),
        ),
    ]
