# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0011_auto_20151011_0751'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phase',
            name='phaseTimeStart',
            field=models.BigIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='phase',
            name='phaseTimeStop',
            field=models.BigIntegerField(default=0),
        ),
    ]
