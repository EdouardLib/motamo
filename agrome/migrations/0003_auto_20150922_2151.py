# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0002_camera'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgromePhase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phaseTime', models.IntegerField(default=0)),
                ('size', models.FloatField(default=None)),
                ('airHumidity', models.FloatField(default=None)),
                ('airTemperature', models.FloatField(default=None)),
                ('hasBeenDone', models.BooleanField(default=False)),
                ('lightParameter', models.ForeignKey(to='agrome.LightParameter')),
            ],
        ),
        migrations.CreateModel(
            name='Cycle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('isOfficial', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Phase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phaseTime', models.IntegerField(default=0)),
                ('size', models.FloatField(default=None)),
                ('airHumidity', models.FloatField(default=None)),
                ('airTemperature', models.FloatField(default=None)),
                ('cycle', models.ForeignKey(to='agrome.Cycle')),
                ('lightParameter', models.ForeignKey(to='agrome.LightParameter')),
                ('waterParameter', models.ForeignKey(to='agrome.WaterParameter')),
            ],
        ),
        migrations.CreateModel(
            name='Plant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60)),
            ],
        ),
        migrations.CreateModel(
            name='Specie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60)),
                ('description', models.TextField(default=b'Not available')),
            ],
        ),
        migrations.AddField(
            model_name='agrome',
            name='dateCreate',
            field=models.DateField(default=datetime.datetime(2015, 9, 22, 21, 51, 23, 899904, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='agrome',
            name='dateUpdate',
            field=models.DateField(default=datetime.datetime(2015, 9, 22, 21, 51, 47, 916802, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='plant',
            name='specie',
            field=models.ForeignKey(to='agrome.Specie'),
        ),
        migrations.AddField(
            model_name='cycle',
            name='plant',
            field=models.ForeignKey(to='agrome.Plant'),
        ),
        migrations.AddField(
            model_name='agromephase',
            name='referencePhase',
            field=models.ForeignKey(to='agrome.Phase'),
        ),
        migrations.AddField(
            model_name='agromephase',
            name='waterParameter',
            field=models.ForeignKey(to='agrome.WaterParameter'),
        ),
    ]
