# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0022_auto_20151115_1300'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lightparameter',
            name='timeStart',
        ),
        migrations.RemoveField(
            model_name='lightparameter',
            name='timeStop',
        ),
    ]
