# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0006_auto_20151006_2256'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='description',
            field=models.TextField(default=b'Not available'),
        ),
        migrations.AddField(
            model_name='cycle',
            name='name',
            field=models.CharField(default='default', max_length=60),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='agrome',
            name='dateCreate',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='agrome',
            name='dateUpdate',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
