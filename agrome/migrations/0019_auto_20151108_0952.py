# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0018_auto_20151106_0209'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agromephase',
            name='lightParameter',
        ),
        migrations.RemoveField(
            model_name='agromephase',
            name='referencePhase',
        ),
        migrations.RemoveField(
            model_name='agromephase',
            name='waterParameter',
        ),
        migrations.RemoveField(
            model_name='phase',
            name='lightParameter',
        ),
        migrations.RemoveField(
            model_name='phase',
            name='waterParameter',
        ),
        migrations.AddField(
            model_name='agrome',
            name='agromeUser',
            field=models.ForeignKey(default=None, to='agrome.AgromeUser', null=True),
        ),
        migrations.AddField(
            model_name='airhumidity',
            name='cycle',
            field=models.ForeignKey(default=None, to='agrome.Cycle', null=True),
        ),
        migrations.AddField(
            model_name='airtemperature',
            name='cycle',
            field=models.ForeignKey(default=None, to='agrome.Cycle', null=True),
        ),
        migrations.AddField(
            model_name='camera',
            name='cycle',
            field=models.ForeignKey(default=None, to='agrome.Cycle', null=True),
        ),
        migrations.AddField(
            model_name='cycle',
            name='currentTime',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='cycle',
            name='isActive',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='cycle',
            name='isModel',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='cycle',
            name='version',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='light',
            name='cycle',
            field=models.ForeignKey(default=None, to='agrome.Cycle', null=True),
        ),
        migrations.AddField(
            model_name='lightparameter',
            name='phase',
            field=models.ForeignKey(default=None, to='agrome.Phase', null=True),
        ),
        migrations.AddField(
            model_name='size',
            name='cycle',
            field=models.ForeignKey(default=None, to='agrome.Cycle', null=True),
        ),
        migrations.AddField(
            model_name='water',
            name='cycle',
            field=models.ForeignKey(default=None, to='agrome.Cycle', null=True),
        ),
        migrations.AddField(
            model_name='waterparameter',
            name='phase',
            field=models.ForeignKey(default=None, to='agrome.Phase', null=True),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='plant',
            field=models.ForeignKey(to='agrome.Plant', null=True),
        ),
        migrations.AlterField(
            model_name='phase',
            name='cycle',
            field=models.ForeignKey(to='agrome.Cycle', null=True),
        ),
        migrations.AlterField(
            model_name='plant',
            name='specie',
            field=models.ForeignKey(to='agrome.Specie', null=True),
        ),
        migrations.AlterField(
            model_name='specie',
            name='family',
            field=models.ForeignKey(to='agrome.Family', null=True),
        ),
        migrations.DeleteModel(
            name='AgromePhase',
        ),
    ]
