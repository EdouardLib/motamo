# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0021_auto_20151111_1614'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lightparameter',
            name='timeStart',
            field=models.DurationField(default=0),
        ),
        migrations.AlterField(
            model_name='lightparameter',
            name='timeStop',
            field=models.DurationField(default=1200),
        ),
    ]
