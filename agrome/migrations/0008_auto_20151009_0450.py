# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0007_auto_20151008_0255'),
    ]

    operations = [
        migrations.AddField(
            model_name='phase',
            name='description',
            field=models.TextField(default=b'Not available'),
        ),
        migrations.AddField(
            model_name='phase',
            name='name',
            field=models.CharField(default='empty', max_length=60),
            preserve_default=False,
        ),
    ]
