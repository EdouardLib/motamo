# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Agrome',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('uid', models.IntegerField(default=None)),
                ('name', models.TextField(default=b'agrome box')),
            ],
        ),
        migrations.CreateModel(
            name='AgromeUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('newsletter', models.BooleanField(default=False)),
                ('experience', models.IntegerField(default=1)),
                ('level', models.IntegerField(default=1)),
                ('avatar', models.ImageField(default=None, upload_to=b'')),
                ('user', models.OneToOneField(default=None, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='AirHumidity',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('agromeUID', models.IntegerField(default=0)),
                ('timestamp', models.IntegerField(default=0)),
                ('value', models.FloatField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='AirTemperature',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('agromeUID', models.IntegerField(default=0)),
                ('timestamp', models.IntegerField(default=0)),
                ('value', models.FloatField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Light',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('agromeUID', models.IntegerField(default=0)),
                ('timestamp', models.IntegerField(default=0)),
                ('color', models.IntegerField(default=1)),
                ('value', models.FloatField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='LightParameter',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('agromeUID', models.IntegerField(default=0)),
                ('color', models.IntegerField(default=1)),
                ('timeStart', models.IntegerField(default=0)),
                ('timeStop', models.IntegerField(default=1200)),
            ],
        ),
        migrations.CreateModel(
            name='Size',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('agromeUID', models.IntegerField(default=0)),
                ('timestamp', models.IntegerField(default=0)),
                ('value', models.FloatField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Water',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('agromeUID', models.IntegerField(default=0)),
                ('timestamp', models.IntegerField(default=0)),
                ('value', models.FloatField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='WaterParameter',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('agromeUID', models.IntegerField(default=0)),
                ('wateringTime', models.IntegerField(default=2)),
                ('waitingTime', models.IntegerField(default=300)),
            ],
        ),
    ]
