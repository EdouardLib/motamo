# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0012_auto_20151011_0907'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phase',
            name='phaseTimeStart',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='phase',
            name='phaseTimeStop',
            field=models.FloatField(default=0),
        ),
    ]
