# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0015_water_agromeuid'),
    ]

    operations = [
        migrations.AddField(
            model_name='family',
            name='image',
            field=models.ImageField(default=None, null=True, upload_to=b'preview'),
        ),
        migrations.AddField(
            model_name='plant',
            name='image',
            field=models.ImageField(default=None, null=True, upload_to=b'preview'),
        ),
        migrations.AddField(
            model_name='specie',
            name='image',
            field=models.ImageField(default=None, null=True, upload_to=b'preview'),
        ),
    ]
