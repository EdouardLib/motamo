# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0013_auto_20151022_1432'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lightparameter',
            name='agromeUID',
        ),
        migrations.RemoveField(
            model_name='water',
            name='agromeUID',
        ),
        migrations.RemoveField(
            model_name='waterparameter',
            name='agromeUID',
        ),
        migrations.AddField(
            model_name='lightparameter',
            name='intensity',
            field=models.IntegerField(default=75),
        ),
        migrations.RemoveField(
            model_name='agromephase',
            name='lightParameter',
        ),
        migrations.AddField(
            model_name='agromephase',
            name='lightParameter',
            field=models.ManyToManyField(to='agrome.LightParameter'),
        ),
        migrations.RemoveField(
            model_name='agromephase',
            name='waterParameter',
        ),
        migrations.AddField(
            model_name='agromephase',
            name='waterParameter',
            field=models.ManyToManyField(to='agrome.WaterParameter'),
        ),
    ]
