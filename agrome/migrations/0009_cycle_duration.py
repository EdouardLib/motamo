# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0008_auto_20151009_0450'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='duration',
            field=models.BigIntegerField(default=2592000),
        ),
    ]
