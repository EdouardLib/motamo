# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agrome', '0020_auto_20151108_1606'),
    ]

    operations = [
        migrations.AddField(
            model_name='cycle',
            name='agrome',
            field=models.ForeignKey(default=None, to='agrome.Agrome', null=True),
        ),
        migrations.AddField(
            model_name='cycle',
            name='isPaired',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='cycle',
            name='user',
            field=models.ForeignKey(default=None, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
