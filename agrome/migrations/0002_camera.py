# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Camera',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('agromeUID', models.IntegerField(default=0)),
                ('timestamp', models.IntegerField(default=0)),
                ('value', models.ImageField(default=None, upload_to=b'')),
            ],
        ),
    ]
