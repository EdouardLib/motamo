# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0024_auto_20151115_1319'),
    ]

    operations = [
        migrations.AddField(
            model_name='agrome',
            name='isServerSynchronised',
            field=models.BooleanField(default=False),
        ),
    ]
