# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0010_auto_20151010_0827'),
    ]

    operations = [
        migrations.RenameField(
            model_name='phase',
            old_name='phaseTime',
            new_name='phaseTimeStart',
        ),
        migrations.AddField(
            model_name='phase',
            name='phaseTimeStop',
            field=models.IntegerField(default=0),
        ),
    ]
