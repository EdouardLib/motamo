# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0005_auto_20151006_2243'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agrome',
            name='dateCreate',
            field=models.DateTimeField(default=0),
        ),
        migrations.AlterField(
            model_name='agrome',
            name='dateUpdate',
            field=models.DateTimeField(default=0),
        ),
    ]
