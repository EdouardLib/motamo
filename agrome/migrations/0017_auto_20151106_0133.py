# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0016_auto_20151105_0029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agrome',
            name='uid',
            field=models.CharField(default=uuid.uuid4, unique=True, max_length=12, blank=True),
        ),
    ]
