# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agrome', '0004_auto_20150927_1412'),
    ]

    operations = [
        migrations.CreateModel(
            name='Family',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60)),
                ('description', models.TextField(default=b'Not available')),
            ],
        ),
        migrations.AddField(
            model_name='plant',
            name='description',
            field=models.TextField(default=b'Not available'),
        ),
        migrations.AddField(
            model_name='specie',
            name='family',
            field=models.ForeignKey(default=1,to='agrome.Family'),
            preserve_default=False,
        ),
    ]
