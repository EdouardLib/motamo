#!/usr/bin/env python
# -*- coding: utf-8 -*-
# µ__author__ = 'Edouard'
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from motamo.models import *
from django.http import response
from django.shortcuts import redirect, render
import datetime
from motamo.forms import *
from django.core.mail import send_mail


def index(request, hasReserved = False):
    today = datetime.datetime.today()

    # TODO: order by date
    dates = Date.objects.filter(date__gte = today).filter(available_places__gte = 0).all()

    return render(request, 'index.html', locals())

def guestList(request, dateId = 0):
    date = Date.objects.filter(id=dateId).first()
    reservations = Reservation.objects.filter(date=date).all()
    return render(request,'guest_list.html',locals())

def reservation(request, motamoDateId = 0):
    form = ReservationForm()
    date = Date.objects.filter(id=motamoDateId).first()

    if request.method == 'POST':
        form = ReservationForm(request.POST)

        if form.is_valid():

            # Create reservation
            reservation = Reservation()
            reservation.name = form.cleaned_data["name"]
            reservation.email = form.cleaned_data["email"]
            #date.image = form.cleaned_data["image"]
            reservation.adult = form.cleaned_data["adult"]
            reservation.children = form.cleaned_data["children"]
            reservation.total_price = reservation.adult*date.price_adult + reservation.children*date.price_children
            reservation.date = date
            reservation.save()

            # Update available places
            date.available_places -= reservation.adult +reservation.children
            date.save()

            # Send an email

            message = """

            Bonjour """ + reservation.name + """

            Votre réservation pour """+ date.name +""" du """ + date.date.__str__() + """ vous est confirmée.

            """ + str(reservation.adult) + """ adult(s)
            """ + str(reservation.children) + """ enfant(s)

            Total à payer à l'entrée : """ + str(reservation.total_price) + """€

            "le spectacle se déroulera à Bruxelles, rue Félix Hap n°11. Les portes ouvriront à 20h avant quoi nous vous invitons à venir casser la croûte (petite restauration sans réservation) dans la magnifique serre du Bouche à Oreille.
            Bien à vous,
            L'équipe Motamo!
            """

            send_mail('Réservation Motamo', message, 'infomotamo@gmail.com',[reservation.email], fail_silently=False)

            return redirect('http://www.motamo.be/#!reservation/vw0hr')

    return render(request,'reservation.html',locals())

def dates(request):
    return render(request,'index.html',locals())


def admin(request):

    form = DateForm()

    if request.method == 'POST':
        form = DateForm(request.POST)

        if form.is_valid():
            date = Date()
            date.name = form.cleaned_data["name"]
            date.description = form.cleaned_data["description"]
            #date.image = form.cleaned_data["image"]
            date.price_children = form.cleaned_data["price_children"]
            date.price_adult = form.cleaned_data["price_adult"]
            date.adresse = form.cleaned_data["adresse"]
            #date.email_template = form.cleaned_data["email_template"]
            date.date = form.cleaned_data["date"]
            date.available_places = form.cleaned_data["available_places"]
            date.is_actif = form.cleaned_data["is_actif"]
            date.save()


    dates = Date.objects.all()

    return render(request,'admin.html',locals())

def deleteDate(request):

    if request.method == 'POST':
        dateId = request.POST.get('id', None)
        date = Date.objects.filter(id=dateId).first()
        date.delete()
        return response.HttpResponse("",content_type="application/json")

def deleteReservation(request):

    if request.method == 'POST':
        reservationId = request.POST.get('id', None)
        reservation = Reservation.objects.filter(id=reservationId).first()
        date = Date.objects.filter(id=reservation.date.id).first()
        date.available_places += reservation.adult + reservation.children
        date.save()
        reservation.delete()
        return response.HttpResponse("",content_type="application/json")
