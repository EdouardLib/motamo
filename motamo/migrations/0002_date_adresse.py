# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('motamo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='date',
            name='adresse',
            field=models.CharField(default='rue F\xc3\xa9lix Hap 11, 1040 \xc3\xa0 Bruxelles', max_length=800),
            preserve_default=False,
        ),
    ]
