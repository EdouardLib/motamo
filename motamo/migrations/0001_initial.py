# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Date',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.CharField(max_length=800)),
                ('price_children', models.FloatField()),
                ('price_adult', models.FloatField()),
                ('email_template', models.CharField(max_length=800)),
                ('date', models.DateField()),
                ('available_places', models.IntegerField(default=1)),
                ('is_actif', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('email', models.EmailField(max_length=254)),
                ('total_price', models.FloatField()),
                ('adult', models.IntegerField(default=0)),
                ('children', models.IntegerField(default=0)),
                ('date', models.ForeignKey(default=None, to='motamo.Date', null=True)),
            ],
        ),
    ]
