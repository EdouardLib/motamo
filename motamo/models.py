__author__ = 'Edouard'
from django.db import models
from django.contrib.auth.models import User
import datetime


class Date(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=800)
    #image = models.ImageField(default=None,upload_to="motamo")
    price_children = models.FloatField()
    price_adult = models.FloatField()
    email_template = models.CharField(max_length=800)
    date = models.DateField()
    available_places = models.IntegerField(default=1)
    is_actif = models.BooleanField(default=True)
    adresse = models.CharField(max_length=800)

class Reservation(models.Model):
    date = models.ForeignKey(Date, default=None, null=True)
    name = models.CharField(max_length=200)
    email = models.EmailField()
    total_price = models.FloatField()
    adult = models.IntegerField(default=0)
    children = models.IntegerField(default=0)


