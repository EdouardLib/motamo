#!/usr/bin/env python
# -*- coding: utf-8 -*-
# µ__author__ = 'Edouard'
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from motamo.models import *
from django.db import models
from django.contrib.auth.models import User
from django import forms
from motamo.models import *

# Create your models here.

class DateForm(forms.ModelForm):
    class Meta:
        model = Date
        fields = ["date","name","description","price_children","price_adult","available_places","is_actif","adresse"]
        widgets = {
            'date': forms.DateInput(attrs={'type':'date'}),
            'description': forms.Textarea(),
            'adresse': forms.Textarea(),
        }
        labels = {
            "name" : "Nom",
            "price_children" : "Prix enfant",
            "price_adult" : "Prix reduits",
            "available_places" : "Places disponibles",
            "is_actif" : "Actif"
        }

class ReservationForm(forms.ModelForm):
    class Meta:
        model = Reservation
        fields = ["name","email","adult","children"]

        labels = {
            "name" : "Nom",
            "email" : "Adresse email",
            "adult" : "Adultes",
            "children" : "Prix réduits"
        }